"""
Kalman filtering based on Dan Simon's article
http://aug-roma.wdfiles.com/local--files/progetti:arpinpero/Kalman_filtering.pdf

"""
import numpy as np
import matplotlib.pyplot as plt


def kalman(velocities, params, time, fixed_u = False):
	kp, ki, kd = params

	T = time
	h = 0.01

	n = round(T / h)  # number of iterations

	v_star = np.empty(n)
	k = np.shape(velocities)[0]

	for i in range(n):
		v_star[i] = velocities[min(i // (n // k), k - 1)]

	position_noise = 2		# position measurement noise [m]
	acceleration_noise = 0.1    # acceleration noise [m/s^2]

	t = np.arange(0, T, h)

	"""
	Model: 
		x(i+1) = Ax(i) + Bu(i) + w(i)
		y(i) = Cx(i) + z(i)
		
		x - state [p, v]^T
		y - measured output
		w - process noise
		z - measurement noise
	"""
	A = np.array([[1, h], [0, 1]])			# transition matrix
	B = np.array([[(h ** 2) / 2], [h]])  	# input matrix
	C = np.array([[1, 0]])					# measurement matrix

	x = np.array([[0], [0]])				# initial state vector
	x_est = np.array(x)						# initial state estimate

	Sz = position_noise**2				# position measurement error covariance
	Sw = acceleration_noise**2 * np.array([[h ** 4 / 4, h ** 3 / 2], [h ** 3 / 2, h ** 2]])  # process noise covariance
	P = np.array(Sw)					# initial estimation covariance

	# arrays to store data from simulation
	state_true = np.empty((n, 2, 1)) 	# true state of a car
	state_est = np.empty((n, 2, 1))   # estimated state
	pos_meas = np.empty((n, 1)) 	# measured position

	state_true[0] = np.array(x)
	state_est[0] = np.array(x_est)
	pos_meas[0] = x[0] + np.random.randn()

	u = np.empty(np.shape(t))			# acceleration values
	e = np.empty(np.shape(t))			# error values
	e_sum = 0							# integral of error
	e[0] = v_star[0] - x_est[0]
	e_sum += e[0] * h

	u[0] = kp * e[0] + ki * e_sum

	u[0] = max(-3.4, min(4.5, u[0]))

	if fixed_u:
		u[0] = 4

	for i in range(n - 1):
		# system simulation
		process_noise = acceleration_noise * np.array([[(h ** 2 / 2) * np.random.randn()], [h * np.random.randn()]])
		x = A @ x + B * u[i] + process_noise

		# measurement simulation
		mean_noise = position_noise * np.random.randn()
		y = C @ x + mean_noise

		x_est = A @ x_est + B * u[i]
		Inn = np.array(y - C @ x_est)
		s = np.array(C @ P @ C.T + Sz)
		K = np.array(A @ P @ C.T @ np.linalg.inv(s))
		x_est = x_est + K @ Inn
		P = A @ P @ A.T - A @ P @ C.T @ np.linalg.inv(s) @ C @ P @ A.T + Sw

		e[i + 1] = v_star[i + 1] - x_est[1]
		e_sum += e[i + 1] * h

		u[i + 1] = kp * e[i + 1] + ki * e_sum + kd * (e[i + 1] - e[i]) / h

		u[i + 1] = max(-3.4, min(4.5, u[i + 1]))		# saturation

		if fixed_u:
			u[i + 1] = 4

		# save for plotting
		state_true[i + 1] = np.array(x)
		pos_meas[i + 1] = y
		state_est[i + 1] = np.array(x_est)

	return state_true, state_est, pos_meas, e, t, v_star, u


def print_kalman():
	v_vals = [20]
	state, state_est, measures, _, t, _, _ = kalman(v_vals, (0, 0, 0), 1, fixed_u=True)

	plt.xlabel('czas [s]')
	plt.ylabel('położenie [m]')
	plt.plot(t, state[:, 0], color='green', linewidth=0.75, label='model')
	plt.plot(t, measures, color='black', linewidth='0.5', label='pomiary')
	plt.plot(t, state_est[:, 0], linewidth=0.75, color='red', linestyle='--', label='estymacja')
	plt.legend(loc='upper center')
	plt.show()


def regulate():
	params = (1, 0.1, 0)
	params2 = (1, 0.1, 0.1)
	v_vals = [14, 27, 41, 33, 14, 25]

	state, state_est, measures, errors, t, v_star, accel = kalman(v_vals, params, 100)
	_, _, _, _, _, _, accel2 = kalman(v_vals, params2, 100)  # used to plot difference between params

	f, axes = plt.subplots(3, 1)

	axes[0].plot(t, state_est[:, 1], label='estymowana')
	axes[0].set_ylabel('prędkość [m/s]')
	axes[0].plot(t, v_star, linestyle='--', linewidth=0.75, label='zadana')
	axes[0].legend(loc='best')

	axes[1].plot(t, accel)
	axes[1].set_ylabel('sterowanie (a [m/s^2])')

	axes[2].plot(t, errors)
	axes[2].axhline(y=0, color='red', linestyle='--', linewidth=0.75)
	axes[2].set_ylabel('błąd sterowania')
	axes[2].set_xlabel('czas [s]')
	plt.show()

	# used to plot differences between params
	# plt.plot(t[400:1000], accel2[400:1000], label='PID')
	# plt.plot(t[400:1000], accel[400:1000], label='PI')
	# plt.ylabel('sterowanie')
	# plt.xlabel('czas [s]')
	# plt.legend()
	# plt.show()


# print_kalman()
regulate()

